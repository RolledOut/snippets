# On automated testing

Testing, a necessary part of software development, can be can be quite a slow and laborious process if you're a human - also very easy to overlook how changes in one part of the codebase may have affected another part. Well what if we could automate that...!

I'll be the first to admit that I hadn't really looked in to automated testing for software until just somewhat recently - mostly I didn't know where start or what to test (which sounds silly in hindsight but eh). Test coverage for Rolled Out! is still fairly small right now, but since we're in the middle of *a lot* of code cleanups (yes, still - this takes even longer than I thought it would), it'd make sense to put in a little extra effort to make sure systems like the stage loader or the local stage database don't explode when you, say, try and load a stage with a malformed PNG texture, or when the game tries to index a stage with invalid metadata.

<script id="asciicast-IwgBOkstWC2NO8smLkw2NsaZ6" src="https://asciinema.org/a/IwgBOkstWC2NO8smLkw2NsaZ6.js" async></script>
(That terminal screencast can also be viewed at [https://asciinema.org/a/IwgBOkstWC2NO8smLkw2NsaZ6](https://asciinema.org/a/IwgBOkstWC2NO8smLkw2NsaZ6) if it looks slightly wrong here)

Certainly not as pretty as, say, [Factorio's testing](https://factorio.com/blog/post/fff-62), but hey it's something.

So why today do I decide to bring this up? Well we've just had a regression (a formerly passing test that now fails) and I feel like I'm back to fighting C++ again *wooo*. You may remember from [dev update 10](https://blog.rolledoutgame.com/2019/12/01/dev-update-10/) that we have our own hand-rolled reflection system, to try and make C++ a bit more dynamic. That has been relying on a rule in C++ for us to differentiate types: `static` data only exists once.

*...except when it doesn't...*

```cpp
    template<typename T>
    static const FTypeTag* From()
    {
        static FTypeTag Tag = FTypeTag( // <--- static data!
            FMetaType<T>::GetName(),
            sizeof(T),
            FMetaType<T>::GetObjCopyConstructFuncPtr(),
            FMetaType<T>::GetObjCopyAssignFuncPtr(),
            FMetaType<T>::GetObjMoveConstructFuncPtr(),
            FMetaType<T>::GetObjMoveAssignFuncPtr(),
            FMetaType<T>::GetObjDestroyFuncPtr(),
            FMetaType<T>::GetObjGetMemberFuncPtr(),
            FMetaType<T>::GetClassType(),
            FMetaType<T>::GetValueType()
        );
        return &Tag;
    }
```

The way templates work in C++, is that you can write some generic code, and then when compiling, the generic code will be duplicated for every type you want to use it with, and the placeholders will filled in. So calling, for example, `FTypeTag::From<uint8>()`, `FTypeTag::From<FString>()`, and `FTypeTag::From<FSceneNode>()`, would duplicate the above code block three times, each time specialized to operate on the types `uint8`, `FString`, and `FSceneNode`. This was great, since we could store static data inside that function, and we'd know that there is only that one piece of data for each type we wanted to work with...

...and then I split our tests out into their own module, separate from the main game (where a module is a DLL, DYLIB, or SO file on Windows, Mac, and Linux respectively).  Usually, with static data, this would be fine, as only one instance of the static data would ever exist, stored somewhere in exactly *one* of the modules. When working with templates however, templated code isn't de-duplicated across modules. It's possible to end up with duplicate data if both our main game module, and our test module, are loaded at the same time, so that's fun.

Now at this point, you might be expecting me to explain how I solved this, although at the moment I'm still trying to figure out the best way to go about handling this. Should we allow duplicate type tags but implement an `operator==` to check for equality? If so, how should be check that? Name comparison? Or could we do some macro trickery to define static data outside of templates with some funky generated name, and ensure that the static data only exists in the main game module? I'm still toying around with how to handle this.

Anyways, that's all from me for now - back to head scratching.
