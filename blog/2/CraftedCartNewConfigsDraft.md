# Big changes - slow progress
So last update I mentioned about really needing to make some cleanups to the code base. There I mostly focused on just generally refactoring the code to remove our synchronization and multiplayer woes.

 Notably, one of the points I briefly mentioned last time was...

> Baking fixed stage transforms into the vertex data on import, rather than transforming it after-the-fact (which makes for some *fun* for the physics crew)

I figured I'd start the cleanups by trying to fix stage import transforms, and then... well, you know how things go sometimes where one thing leads to another, which leads to another.

Redoing stage loading pretty much entirely is something that had been planned for a while. Since we were going to chop up the code base anyway, I decided that now would be a good time to start doing this.

## New stage config format!
![New config render](https://gitlab.com/RolledOut/snippets/raw/master/blog/2/new_config_render.png)

Oh yeah I wasn't kidding when I said redoing this pretty much entirely.

Before I get into the details of what we have in the works for the new config, let's go over what was wrong with our current one. If you're not familiar with how the current configs look, here's a couple snippets from a [static](https://gitlab.com/RolledOut/snippets/blob/master/blog/2/old_simple_config.xml) and an [animated](https://gitlab.com/RolledOut/snippets/blob/master/blog/2/old_animated_config.xml) one.

### Why is it bad?

#### One: It's XML
XML is... not great for configs for a couple of reasons. Firstly XML is slow to parse... like really slow. A while back I did a few benchmarks just to see how slow parsing XML is, compared to a couple other formats. I took all three-hundred-and-something of our XML stage configs and converted them (badly) to JSON and MsgPack. These were all compressed with gzip as well as we were experimenting with ways to save on file size.

The test was simple: I asked Python to decompress and parse every stage config as quickly as it could. In between all the tests I cleared the disk caches to ensure that I/O speeds were more-or-less fair for every run.

| Format      | Time to parse |
|-------------|---------------|
| .xml.gz     | 9.14s         |
| .json.gz    | 3.65s         |
| .msgpack.gz | 2.77s         |

Yup. XML is slow alright.

In addition, XML is **insecure**. There are ways to exploit the XML format that can easily cause a computer to start hurting. One of the more notable ones is the [billion laughs attack](https://en.wikipedia.org/wiki/Billion_laughs_attack). XML lets you define "shortcuts" that can expand out into multiple other entities when parsing, which can expand again, and again, and again, etc. As you may imagine, this makes easy to create a billion entities from just a few, consuming large amounts of memory and CPU time to parse.

"Well just download stages from trusted sources," I hear you say. Well hold on a few moments before you try to dismiss me. Our server will have to parse configs too, meaning the scope of this attack is not just restricted to you, the player.

Note that there are various other vulnerabilities in XML too, but just this one should be enough to help you understand why it may not be such a good choice. In the end, we settled on JSON. While MsgPack is faster to parse, the difference is negligible and JSON is far easier for humans to read and modify.

#### Two: There's no curves
XML uses \<pointy angle brackets\> everywhere...

Ok terrible joke aside, *all* keyframe interpolation is linear - there's no way to define keyframes with other kinds of interpolation with our current configs. This lead to some very, very messy configs with thousands of keyframes, just to recreate animation curves set-up in Blender.

![Baked keyframes in Blender](https://gitlab.com/RolledOut/snippets/raw/master/blog/2/baked_keyframes.png)

Each one of those orange dots represents a keyframe in the config... and all this just for a simple curve! Wouldn't it be much better if we could just define this in, say, three keyframes.

![Unbaked keyframes in Blender](https://gitlab.com/RolledOut/snippets/raw/master/blog/2/unbaked_keyframes.png)

Ah! Much better.

#### Three: Special objects (such as goals) were never defined in the config
If you've dabbled with custom stage creation, you may have noticed how the [Blender plugin](https://gitlab.com/RolledOut/blendtorolledout) creates objects with special tags in their name, such as `[GOAL_B]` or `[BANANA_S]`. These tags aren't just used by the plugin or as a human-readable name for the objects, the game also uses them to determine where to place these special objects. We knew this was a hack from day one, but this hack has stayed in here since then.

Why is this an issue? Well this means special objects are not very flexible at all. There is no way to define custom properties (such as a wormhole destination) for these objects beyond just making a new [TAG] to represent a variant.

### What's new?
Our proposed new stage config format looks a bit like [this](https://gitlab.com/RolledOut/snippets/blob/master/blog/2/new_config.json) at the moment. I'll go ahead and summarize the more notable changes to spare you the time trying to compare walls of configs yourself.

#### One: Metadata and stage configs have been combined into a single file
There's been an amount of confusion about why stages definitions are split over a config and a metadata file. I've always just said that the metadata file is supposed to contain information that makes sense to be searched and indexed, with everything else going in the config file, however this line can be quite fuzzy depending on what users may want to search for and be able to see at-a-glance. Heck even I forget whether the `bonus` flag goes in the config or the meta file.

Combining the two files into one should help reduce the confusion about that goes where. While the new config format does have a `metadata` section in it, this will be restricted to information that cannot be inferred elsewhere from the config, such as the stage name and tags. A stage layout and objects should still remain the same even if its metadata is completely changed. It also means if I ever wish to index information that can be inferred about the stage (such as the number of goals), only a single file would need to be read.

#### Two: Stage names and descriptions can now have translations
![Translation keys](https://gitlab.com/RolledOut/snippets/raw/master/blog/2/new_config_translatable.png)

I don't think I need to explain why this is a benefit.

#### Three: The model file has become a mesh library
The config will now define which objects should be placed in the world and all their parent/child relationships, as opposed to the model file dictating the hierarchy and all objects that should exist. This will help when it comes time to implement the in-game stage editor, as now a config can be used to refer to objects without a bundled model file, such as meshes built-in to the game.

#### Four: Materials are now defined in the config
![Materials](https://gitlab.com/RolledOut/snippets/raw/master/blog/2/new_config_materials.png)

The game will be able to provide multiple base materials (Such as opaque textured, masked transparent, translucent, to name a few examples) with various parameters that can be overridden in the config. The config will also now define what materials different meshes should use. This again will help with an in-game stage editor to enable easy skinning of built-in meshes.

#### Five: Animation curves
![Curves](https://gitlab.com/RolledOut/snippets/raw/master/blog/2/new_config_curves.png)

Heck yeah this should simplify configs a lot if I can just copy keyframe data from Blender without needing to write out a bajillion lines. At the very least, I'd like to support Bezier and linear curves given that the majority of animations you may typically create will be using these curves.

## What's been going on in the code?
Unsurprisingly, I've been writing code to parse this new config format. I've also been writing *a lot* of data structures to contain information about the new configs, and a lot of validation code such that a malformed config won't crash the game any more.

One of the biggest changes in the code is that I can no-longer rely on `UObject`s. Unreal Engine provides these convenient objects which take care of a lot of work for you, such as memory management, and bridging the gap between C++ and Unreal's Blueprint visual scripting. If you're not familiar with Blueprint, an example showing confetti particles spawning on-goal is shown below.

![A Blueprint script to spawn confetti on goal](https://gitlab.com/RolledOut/snippets/raw/master/blog/2/blueprints.png)

Unfortunately `UObject`s are unsafe to use outside of the main game thread, as those automatic memory management conveniences start losing track of things when it comes to multiple threads (It would be disastrous if Unreal decided to free some memory I was in the middle of using). I would really love to have the game be able to load stages on a separate thread, meaning I can pre-load bits of the next stage when playing through a course, as well as hopefully speeding up the incredibly slow stage indexing on first launch. This means I'm going to need to untie some functionality from Blueprint (which should be a good thing in the end as Blueprints are a pain to collaborate on anyways).

## tl;dr

I started trying to clean up stage loading which lead to developing a new stage config. I'm now writing code to take these configs and make them in-game playable stages, which is going slowly due to large amounts of new data structures, validation, and decoupling stuff from Blueprint visual scripts.
