# Model/view separation

If you've done much GUI or web programming before, you may have come across the model-view, or model-view-controller pattern before. Simply put, this pattern separates logic into three distinct segments. The "model" houses the data, the "view" looks at the model and presents it to the user, and the "controller" lets the user do actions such as clicking buttons or entering text, and modifies the model.

![Model-view-controller diagram](https://gitlab.com/RolledOut/snippets/raw/master/blog/12/mvc.png)

Thanks for the [image](https://commons.wikimedia.org/wiki/File:MVC-Process.svg) WikiMedia/RegisFrey

So how does this apply to Rolled Out!? (and how do I use punctuation when the title Rolled Out! itself has punctuation in it? Rolled Out‽). Well we're applying this pattern to the stage itself! We have a model, `FStageData`, which stores the state of the stage (like object transforms, meshes, and such), and we want to sync over this model to a view, such that the stage can be displayed in the world. Lua scripts and stage animations act like a controller in that they can modify the stage data.

![How this pattern applies to the stage](https://gitlab.com/RolledOut/snippets/raw/master/blog/12/mvc_stage_actor.png)

What's the point of splitting the model and the view though anyway? Well aside from keeping code organized, there's another reason: what if you want to simulate a stage without seeing it? If physics ends up being deterministic across devices (which so far, physics determinism is looking pretty good right now), it'd be nice to be able to verify replays haven't been fudged with for leaderboards, by replicating inputs and seeing if the end result is the same (does replaying a player's inputs say that the they goal and get the same time/score as their leaderboard submission claims?). Being able to simulate stages/replays, without having to be tied down with chunks of Unreal's framework that isn't necessary for this (like needing to create a world, load maps, setup a game mode, setup a game state, only being able to access parts from the main thread, etc.) could hugely save on resources.
