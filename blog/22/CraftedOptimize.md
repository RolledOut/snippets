# Daft optimizations

So I've got a funny story this time round. :) If you don't care about all the gory details, there's a tl;dr near the
bottom of this section.

Over the past few days, we've been doing a round of optimizations - seeing where the game is being bottlenecked and trying to get it to run better on lower end machines. So... my first thought was to shove the game through [Callgrind](https://valgrind.org/info/tools.html) - we must be trying to do too much work on the CPU each frame so I'll use Callgrind to track down which places are consuming the most CPU time and see if I can make 'em run a bit faster.

## Doing less work on the game thread...

Callgrind is slow... really slow (just running it tanks the game's framerate from about 60 FPS on my test level down to 0.6 FPS), but that's no issue to me - I just need Callgrind to tell me, relatively speaking what takes the most CPU time in a frame.

![Call graph](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/call_graph.png)

If you've never seen one of these graphs before, what this shows is the paths that code execution takes (starting from the top and drilling down). Each rectangle represents a function, and a bigger percentage numbers in those rectangles means a function took a longer amount of time to complete its work.

We figured most work is done inside `FPlayerBall::Tick`, given the game tended to slow down *a lot* when spawning in a ball, so I jumped there to see what exactly consumes the most time inside that function, aaand unsurprisingly most of it is within physics code. While I'm ok with shuffling physics code around a bit, actually trying to optimize it I was not comfortable with. Regardless, I poked around a bit, `FORCEINLINE`-ing some small but very frequently use functions, trying to avoid many smaller memory allocations, caching physics meshes and animated objects so that they wouldn't have to be queried every frame, and what not. Stage tilting was also offloaded onto the GPU to save the render thread from having to re-figure out the transforms of everything on the stage each time you tilt the stage.

So... did any of that make a difference? Well... yes, but actually no.

At this point, we're gonna have to take a small detour into how Unreal Engine does its updating every frame. In my mind, I had been assuming that the engine would call into game code, giving us a chance to update animations, do physics work, etc. *before* the engine started to render a frame. What *actually* happens is both of these things - updating the game and rendering a frame - happen simultaneously.

Anyways, armed with this new knowledge, I started paying attention to not just the FPS/frametimes, but the amount of time it takes to update the game vs the amount of time it takes for the rendering thread to send stuff to the GPU vs the amount of time it takes the GPU to render a frame. Here's a before and after:

![Before](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/stats_pre_optimize.png)
![After](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/stats_cpu_optimize.png)

So these small optimizations meant game updating took slightly less long by about a millisecond and a half, but it's mostly meaningless (for me, and for our weak test system) given the GPU is the bottleneck here. If you happen to have a particularly weak CPU though, then maybe this might help just a little.

## Doing less work on the GPU...

Naturally, knowing that the GPU was the bottleneck, we started focusing our attention on that. Ok.. let's take a looksie at shader complexity then.

![Shader complexity visualization](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/shader_complexity.png)

Hmm.. looking pretty good here on the whole. The ball could perhaps do with a bit of work but really this is looking
pretty good. At this point, Brandon went ahead did a bit of work on the ball shader, and while he was at he he also took
the chance to reduce the poly count of the ball and Morris. The ball clocked in at 8k triangles, with Morris being a
further 18k! Let's see what Brandon did, shall we.

![After simplifying shaders/reducing poly counts](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/stats_shader_tris_optimize.png)

A...ah.

Ooooook then, how about we look at our draw calls...

![Draw calls](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/draw_calls_before.png)

***Excuse me???*** How the heck are we averaging around 1500 mesh draw calls, that's insane! For those unaware what a draw call is, each draw call is a bit of work being handled by a driver before being sent off to the GPU - the fewer of these you have, the less information the CPU will have to spew off to the GPU.

At this point, I figured I'd try changing the plugin we use to generate meshes on the stage. Unreal ships with a ProceduralMeshComponent which allows us to do this, though I've been aware of the [RuntimeMeshComponent](https://github.com/KoderzUnreal/RuntimeMeshComponent) plugin which claims to do near-enough the same except perform better. I figured maybe, just *maybe* it could reduce draw calls, or at the very least help in some other places.

So.. I slotted the plugin into our game (after patching it a bit because of course nothing wants work right out of the box), replaced all uses of the ProceduralMeshComponent with the RuntimeMeshComponent, aaaand...

![After switching to RuntimeMeshComponent](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/stats_rmc_optimize.png)

Ok, a slight improvement on the `Draw` thread, but overall no dice. Draw calls didn't change either.

At this point I wasn't really quite sure what to do any more. I was just flipping through the Unreal Engine documentation, seeing if there were any other tools built-in to the engine that could give me some insight. Turns out, Unreal has a GPU profiler - sounds useful.

## The daft thing

So.. I whipped out the GPU profiler in the middle of gameplay to see if it'd tell me anything new.

![The GPU profiler - the larger a block is, the more time it spent on the GPU](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/gpu_profile.png)

...aaaaaand it was at this point I started laughing to myself! So so so sooo much time on the GPU had been spent in that `SceneCapture_BP_PlayerBallPawn_C_0` object, it was a wonder that I was still managing to hit 60FPS in normal gameplay, I thought to myself.

A bit of context: a long while ago, Brandon was experimenting with making the ball appear reflective by taking a 360deg capture of the world every frame at the ball, and projecting the captured texture onto the ball. For obvious reasons, rendering the world twice (once for the reflection, and again for display on your monitor) absolutely tanked the performance, so that feature was left disabled. As part of code cleanups, I had been unwiring various scripts that Brandon had made and replacing them with C++ implementations. The way the world capturing worked is that the ball would spawn in with the scene capture component, and Brandon would remove the component on spawn to disable it.

As I had been unwiring everything, the scene capture component then stopped being deleted on ball spawn and such started capturing the world around it on every frame again, except that it never showed up on the ball since I also unwired the logic Brandon had to show ball reflections!

So... after *not* rendering the entire scene twice per frame, what are our final results?

![The final statistics](https://gitlab.com/RolledOut/snippets/raw/master/blog/22/stats_post_optimize.png)

Hoooh... theeeere we go!

## tl;dr

The tl;dr of this is that in the past, we had been experimenting with ball reflections which required rendering the entire scene twice per frame! We then disabled this by default as, for obvious reasons, it tanked performance. As part of some code cleanup, I had accidentally re-enabled rendering the scene twice, but without showing ball reflections, so I never noticed it. After a bit of investigating, we realized that aaand put an end to it.
