The UI navigation kludge
========================

*Rolling* back around (hah!) to our input system again, something that has never played well with before was the game's UI. Well as of just a few moments ago as I write this, I can say that it plays slightly less worse now! ;P Anyways, the gist is that Unreal has its own input system that its UI is closely tied to, and we have our own thing going on since we want to support a wider range of controllers - somehow we needed to glue the two together a bit. What follows is just a dump of thoughts I had and how eventually we got it to kinda-mostly work.

## Idea 1: Pull a Tom Scott

If you haven't seen Tom's video on how he [bodged together an emoji keyboard](https://www.youtube.com/watch?v=lIFE7h3m40U), what basically happens is when you press a key on a keyboard, a program called LuaMacos intercepts that, figures out which number emoji you want to type, saves it to a file on disk, then presses the F24 key (yes, there's that many function keys). From there, AutoHotkey intercepts the F24 key, reads that file, looks up which emoji to type, and types it. It's ugly but it worked, and I was thinking about how we could maybe do something similar in our game.

So the idea was if you press a directional key, or an accept/back key, or what not, our input system would pick up on that and save the direction/action to a variable somewhere in memory, then simulate a key press that Unreal understands that players almost certainly wouldn't have on their keyboards. Unreal doesn't recognize F24 so maybe something like uhh...  `EKeys::Daydream_Right_Trackpad_Click` - you're not gonna be playing this game on a Daydream headseat anyway.  Then, from the UI scripts, we could listen for that key press, read that variable in memory, and figure out what we're supposed to do from there.

I...never even got as far as writing a single line of code for this idea as I kept digging to see if there was a better way we could handle this, where I found....

## Idea 2: Add our own custom keys, and simulate those key presses

...that it was relatively simple to add your own key types to the engine (with [EKeys::AddKey](https://docs.unrealengine.com/en-US/API/Runtime/InputCore/EKeys/AddKey/index.html), if anyone's curious). So what if instead, I made custom keys for menu navigation in all 4 directions, and just simulated pressing them from our input system. While we're at it, also make it so Unreal should [respond to our custom keys](https://docs.unrealengine.com/en-US/API/Runtime/Slate/Framework/Application/FSlateApplication/SetNavigationConfig/index.html) when figuring out UI navigation, instead of the default arrow keys, gamepad left stick, etc.

So for starters, we create the keys...

```cpp
UCLASS(BlueprintType)
class ROGAME_API UInputManager : public UObject
{
    GENERATED_BODY()

public:
    // Custom keys for menu-ing
    static const FKey KeyPolarLeft;
    static const FKey KeyPolarRight;
    static const FKey KeyPolarUp;
    static const FKey KeyPolarDown;
    static const FKey KeyPolarAccept;
    static const FKey KeyPolarBack;
    static const FKey KeyPolarPrevPage;
    static const FKey KeyPolarNextPage;

    // --snip--
};
```

...then we make a custom navigation config that says UI navigation should respond to these keys we just made...

```cpp
class FPolarNavConfig : public FNavigationConfig
{
public:
    FPolarNavConfig()
    {
        bTabNavigation = false;
        bKeyNavigation = true;
        bAnalogNavigation = false;

        KeyEventRules.Add(UInputManager::KeyPolarLeft, EUINavigation::Left);
        KeyEventRules.Add(UInputManager::KeyPolarRight, EUINavigation::Right);
        KeyEventRules.Add(UInputManager::KeyPolarUp, EUINavigation::Up);
        KeyEventRules.Add(UInputManager::KeyPolarDown, EUINavigation::Down);
    }

    virtual EUINavigation GetNavigationDirectionFromAnalog(const FAnalogInputEvent& InAnalogEvent) override
    {
        return EUINavigation::Invalid;
    }

    virtual EUINavigationAction GetNavigationActionForKey(const FKey& InKey) const override
    {
        if (InKey == UInputManager::KeyPolarAccept) return EUINavigationAction::Accept;
        if (InKey == UInputManager::KeyPolarBack) return EUINavigationAction::Back;
        return EUINavigationAction::Invalid;
    }
};
```

...and finally, we register the keys and config on game startup...

```cpp
void UInputManager::Init()
{
    InitBindings();

    // Define custom keys
    static const FName MENU_CAT = TEXT("PolarKeys");
    EKeys::AddKey(FKeyDetails(KeyPolarLeft, INVTEXT("PolarLeft"), 0, MENU_CAT));
    EKeys::AddKey(FKeyDetails(KeyPolarRight, INVTEXT("PolarRight"), 0, MENU_CAT));
    EKeys::AddKey(FKeyDetails(KeyPolarUp, INVTEXT("PolarUp"), 0, MENU_CAT));
    EKeys::AddKey(FKeyDetails(KeyPolarDown, INVTEXT("PolarDown"), 0, MENU_CAT));
    EKeys::AddKey(FKeyDetails(KeyPolarAccept, INVTEXT("PolarAccept"), 0, MENU_CAT));
    EKeys::AddKey(FKeyDetails(KeyPolarBack, INVTEXT("PolarBack"), 0, MENU_CAT));
    EKeys::AddKey(FKeyDetails(KeyPolarPrevPage, INVTEXT("PolarPrevPage"), 0, MENU_CAT));
    EKeys::AddKey(FKeyDetails(KeyPolarNextPage, INVTEXT("PolarNextPage"), 0, MENU_CAT));

    // Don't use UE4's stock navigation mappings at all
    FSlateApplication::Get().SetNavigationConfig(MakeShared<FPolarNavConfig>());
    // FSlateApplication::Get().SetNavigationConfig(MakeShared<FNullNavigationConfig>());
}
```

...oh and, don't forget to unregister the keys when the game ends.

```cpp
void UInputManager::OnEnd()
{
    EKeys::RemoveKeysWithCategory(TEXT("PolarKeys"));

    PolarInput::TearDown();
}
```

Sweet, now we've got that in-engine, we just need to simulate pressing `KeyPolarLeft`, `KeyPolarAccept`, etc.  Uhh.....how?

Well maybe we can call [FSlateApplication::OnKeyDown](https://docs.unrealengine.com/en-US/API/Runtime/Slate/Framework/Application/FSlateApplication/OnKeyDown/index.html) to simulate a key press... except that takes a key code and a character code, which our custom keys don't have. How about [FSlateApplication::ProcessKeyDownEvent](https://docs.unrealengine.com/en-US/API/Runtime/Slate/Framework/Application/FSlateApplication/ProcessKeyDownEvent/index.html)? Hrm, looks like it also needs a key code/character code, but it *also* needs an `FKey`, which I have! Soo maybe I could get away with this if I just say the key/character codes are zero?

Weeelll that..didn't work. The engine does seem to pick up on the key presses, though they didn't seem to affect UI navigation at all. Ooook, next idea!

## Idea 3: Force UI navigation myself, instead of trying to trigger it through key presses

...seems like an obvious solution to any programmers who may be reading this, right? ...right? The issue here is that navigation requests are typically done by each UI component itself as a reply to...*some* system after handling a key down event. It looked like a lot of the ways Unreal pokes at UI navigation was private that I couldn't touch, but eventually after a lot more digging around, I found [FSlateApplication::ProcessReply](https://docs.unrealengine.com/en-US/API/Runtime/Slate/Framework/Application/FSlateApplication/ProcessReply/index.html).  So now the idea was how could I fake a "reply" to get the engine to navigate in the UI for me.

Well faking the reply itself is fairly simple, I just make a reply and tell it I want to go in *that* direction, from the currently focused UI widget, and pretend it comes from the keyboard...

```cpp
FReply::Handled().SetNavigation(Direction, ENavigationGenesis::Keyboard, ENavigationSource::FocusedWidget)
```

...what's trickier is the needed data surrounding that that this `ProcessReply` function needs. Most notably, I need to figure out what the currently focused UI widget is. That'll be simple, right? There's probably just a function somewhere that'll tell me what's focused, right?

Weeelll... yes and no. While for most desktop purposes it might make sense for only one widget to be focused at a time ever, Unreal lets multiple widgets have focus, one per user. Perhaps a bit surprising at first but I guess it makes sense if you want multiple players to both control bits of the UI at once (say, for example, in the character picker in Smash). Anyways, to get the focused widget here, I kind-of make some guesses here...

```cpp
TSharedPtr<SWidget> FocusedWidget = App.GetUserFocusedWidget(0);
if (!FocusedWidget) FocusedWidget = App.GetKeyboardFocusedWidget();
if (!FocusedWidget) return;
```

...first checking if user ID 0 has a focused widget, if not then we see what the keyboard user has focused, and failing that, we just give up.

Anyways, the other bits of data needed for `ProcessReply` are fairly easy - we need...
- The widget underneath the mouse cursor, which we don't care about do I give it null
- A mouse event, which we also don't care about so I give it null again
- And a user ID, which I just say is `0`

aaand finally, after all of that faff, simulating UI navigation works!

<del>...unless you're using the arrow keys *GOSH DARN IT*</del> nevermind, of course I find it just as I'm done writing this, and when I say I find it, I mean I have absolutely no idea what I did but it works now.
