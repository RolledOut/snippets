# I feel Powerful

I didn't really have much of interest to say for the last few weeks, mostly boring backend stuff and bashing my head against a wall and getting nowhere >.<. This time though, hooh I have something fancy looking.

<video controls muted playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/34/PowerfulMaterials.mp4" type="video/mp4">
</video>

So, while we had conveyor surfaces in the game, we didn't really have a way to *convey* (ha ha) to the player whether a surface was a conveyor or not, such as by having the texture scroll. The way I saw it, there were two ways we could handle this: we could add a simple X/Y scroll speed option to materials, oooor I could go and wire up our animation system to materials. ...yeah I went with the latter. :)

And so now, every material property that could could manipulate for custom stages, can now also be hooked up to animation curves (excluding textures... you can't exactly animate a texture with a curve). Want to animate texture scroll? You'll want an animation that uses the path `parameter_overrides.vector2.tex_coord_offset.x` (or `.y`). Opacity? No problem, as long as you're using a translucent material. Refraction? You can make some trippy looking things with that. :3

(Can you tell I'm having perhaps a bit too much fun with material animations?)

Anywyas, we'll have technical docs for all the properties you can use and how you can animate them, I promise, so those of you who may like making custom level can muck around with this.
