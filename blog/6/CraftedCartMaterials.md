# The new material system

Ah materials, the magical layer of paint that goes on-top of your objects to control the appearance of their surfaces. If you've had a chance to create custom stages in the Rolled Out! beta, you may be aware of the current material system we use to handle stage surfaces. It looks a bit like this.

![The current stage material](https://gitlab.com/RolledOut/snippets/raw/master/blog/6/current_stage_mat.png)

Here we have five textures feeding into six properties of the material, with the material being opaque-only, aaand that's all you've got. Want any transparency? Too bad. Want to make a surface fully metallic? You have to bother creating a white texture. How about animating UVs to emulate a conveyor effect? Nope.

Now while I would love to tell you that our new material system has the same flexibility as Unreal Engine's own material editor, where you can create all sorts of nodes and connect 'em together...

![A more complex water material](https://gitlab.com/RolledOut/snippets/raw/master/blog/6/water_morris.png)

...well, that isn't all that feasible to do in-game. As part of the packaging process for the game, materials get compiled into platform-specific bits of shader code, and the nice material editor we had is gone. If we take a look at the Rolled Out! install on my Linux box, we can see that we have GLSL and Vulkan shader caches, for the OpenGL and Vulkan renders respectively.

```sh
$ pwd
[redacted]/.steam/steam/steamapps/common/rolledout/Engine

$ ls | grep Shader
GlobalShaderCache-GLSL_430.bin
GlobalShaderCache-SF_VULKAN_SM5_NOUB.bin
```

While technically it would be possible to compile shader code for stages while the game is running ([At least one Redditor has done it](https://www.reddit.com/r/unrealengine/comments/63tc7g/show_off_hotloading_custom_hlsl_shaders_at_runtime/)), we would have to maintain code to generate or convert between HLSL, MSL, GLSL, and SPIR-V code (for Microsoft's Direct3D, Apple's Metal, OpenGL, and Vulkan) to keep the game cross-platform. In addition we'd need to figure out to bypass chunks of Unreal's shader code and still be able to hook into it to be able to apply the shaders to objects, something the Redditor described as "a massive hack".

Sooo, that's not exactly something I'm willing to do.

Instead, we're building on our original material system. For starters, instead of having only one base material (forced to be opaque and have a few texture parameters), we have a variety of base materials to build off of now, including ones for opaque surfaces, translucent surfaces, masked surfaces (a surface that can have bits with either 0% or 100% transparency, but nothing in-between), as well as a few stock materials that can't be customized (A default grid, the gravity surface material, and an invisible material).

![The current base material selection](https://gitlab.com/RolledOut/snippets/raw/master/blog/6/material_spheres.png)

In addition, all texture parameters that were exposed now have scalar or vector multipliers (with the exception of the normal map), with the default textures of these being white squares now. This means it's easy to, say, make an object glow blue now without having to create a texture for it. We can also go beyond the 0.0 - 1.0 range for HDR colors with multiplier values too (useful for making stuff look extra-glowy, as Unreal will add a bloom around incredibly bright colors).

![The new setup for the rolledout:mat_surface_opaque_lit_generic material base](https://gitlab.com/RolledOut/snippets/raw/master/blog/6/generic_opaque_mat.png)

![oooh, glowy](https://gitlab.com/RolledOut/snippets/raw/master/blog/6/blue_glow_mat.png)

The config for this material would look something like this...

```json
{
  "name": "mat_glowy",
  "base_material": "rolledout:mat_surface_opaque_unlit_generic",
  "parameter_overrides": {
    "emissive_multiplier": {
      "type": "vector3",
      "value": {"x": 5.49998, "y": 34.7542572, "z": 50.0}
    }
  }
}
```

Something I would also like to look in to in the near future would be letting animations modify material properties. One practical use for this could be to animate the UVs of an object, to emulate a conveyor belt.

<video controls autoplay muted loop playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/6/scrolling_uvs.mp4" type="video/mp4">
</video>

Anyways, that's all I have for this post. Back to ~~the studio~~ Brandon.
