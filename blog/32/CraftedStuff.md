# Titling is hard

Hey, welcome to the new year! I hope you all had a good holiday season :)

Stuff's been winding down around this time of year, so there's not gonna be too much from me here (eehe.. I feel like I'm saying this on almost every blog post we do now >.<). Hoow about we start over here.

<video controls muted playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/32/RoSpringAnim.mp4" type="video/mp4">
</video>

A little touch, but springs now animate in the direction of their impulse. Not really a lot more to say about this so let's move on to...

<video controls muted playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/32/RoCameraPreview.mp4" type="video/mp4">
</video>

More recently, I've been making a little preview for your camera settings ~~and really not having a fun time with drawing custom UI in Unreal (woo for a complete lack of documentation!)~~. As you might imagine, trying to tinker with your camera settings without being able to see what you're doing isn't the easiest thing to do - setting all the sliders to their extremes can even leave the camera in weird places such as *below* the ground! Hopefully this helps a bit, though I'll certainly need to prettify it up some first. ;P
