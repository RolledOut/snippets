# Which way is up again?

![Wuh oh, the box fell over](https://gitlab.com/RolledOut/snippets/raw/master/blog/11/this_way_up.png)

That's actually how Rolled Out! stages are made... I think... probably. We might need a version 3 of the stage-o-matic 5001, it doesn't seem to know its lefts from its rights, and the conveyor output is too high such that packages tilt when they fall off of them.

Ok jokes aside, I've been tinkering with the *old* stage loading code again, given that there were some issues with that. As much as I'm willing to abandon the old code, the new physics branch of our game is still reliant on the old code. One of the notable issues was our long-outstanding "solution" for stages being the wrong way up, and also flipped, when they were imported.

But first, a quick primer on co-ordinate systems. Various different 3D applications can have different co-ordinate systems: some of them like to use Y as the up-axis, others like to use Z for up. In addition, some like to have positive X as the right axis, and are called "right handed", while others have positive X as the *left* axis, and are "left handed".

As you may be able to imagine, converting between these isn't the most pleasant experience. For our process, we model stages in Blender, a right-handed Z-up 3D modeler. These are then exported as FBX files and imported into the game using Assimp, which is right-handed Y-up. Finally, we take the data that Assimp has given us and transform it a bit for Unreal Engine, which is left-handed Z-up.

![The three co-ordinate systems we have to work with](https://gitlab.com/RolledOut/snippets/raw/master/blog/11/axes.png)

The easy solution, which is what we were doing before, was just to rotate everything by 90 degrees, and scale it by -1 on the X axis. This isn't exactly the most ideal solution, it'd be better if we could fix the vertex data of the meshes, and their transforms, but it worked well enough for the old physics.

As it turns out... this is trickier that it sounds - especially when you have objects parented to other objects. In addition, I also have to fix up transforms in the config exporter plugin, to convert between Blender's co-ordinate system to Unreal's one. Anyways, some twiddling around with both rotations and axis flipping and stuff later, we have stuff *mostly* working in-game now, all ready for the physics team to work with.

Anyways, back to being bummed that [packaging failed for a game jam submission](https://itch.io/jam/yogscast-game-jam/topic/633412/the-jaffa-factory-the-game-that-never-was).
