# Un-modding Unreal Engine

One of the things we did quite a while ago was modify Unreal Engine a bit to use [SDL2](https://www.libsdl.org/) on Windows to handle controller inputs - the reason for this being that Unreal only handles controllers that make use of "XInput", whereas we wanted to support "DirectInput" as well such that a wider range of controllers will work with the game.

## A quick-ish primer on XInput and DirectInput

You've probably heard of "DirectX" before - it's a collection of code libraries and APIs provided by Microsoft to handle various multimedia tasks. Most likely you'll be familiar with DirectX as *that thing games need in order to render frames*, but that's just one component of DirectX: Direct3D.

Anyways, DirectInput is a library that's been in DirectX since version 1 in 1995 - you can use it to get a list of connected controllers/joysticks, figure out what joysticks/buttons they have, and take it from there.

XInput came along in 2005 as a new way to handle controller devices. While perhaps easier to work with (mostly in the sense that you don't have to deal with input mappings any more), it's a fair bit more restrictive about what it supports - most notably: XInput seems to revolve around the idea of every controller looking kinda similar to an Xbox one. It's still not too hard to find controllers today that don't support XInput as well: take the DualShock 4 for example - that only works with DirectInput.

So... despite Microsoft's lack of DirectInput support (it's deprecated now!), we still consider it important to support controllers that don't look like an Xbox one. SDL2 is an interface I'm familiar with and nicely wraps up both DirectInput and XInput into an easy to use API (and with SDL2 being cross-platform, if we run into similar "not-all-controllers-work" situations on macOS/Linux, it wouldn't be too hard to use SDL2 for those platforms too).

## Our implementation

When I first went about implementing this, I figured it'd be nifty to modify the engine for it. *Oh if I modify the engine, I can hook SDL2 input right into Unreal's input system - wouldn't that be neat?*

Weeelll that never happened. SDL2 support was added to the engine but I just wired the game to grab controller inputs directly from there instead of routing anything through Unreal's input. Besides, I'm, not sure how that'd even work given it seems Unreal doesn't support arbitrarily shaped controllers in its input mappings.

What ended up happening is the controller code ended up being a big source of crashes that I was always hesitant to look in to, since modifying it often times would require me to recompile half of the entire engine - safe to say, that is not a quick process.

So... just a couple of days ago I trashed the Unreal Engine install on my Windows box and downloaded a clean copy right from the Epic launcher - one without our modifications. A new plugin was created and I ported over all the modifications I made to the engine over to there. A little bit of initialization/shutdown glue later, aaand the game now builds with a stock engine! That should also help with debugging crashes when it won't take an hour and a half to recompile changes.

And Brandon can now hush about builds taking so long since you don't need to compile the whole engine any more. :)
