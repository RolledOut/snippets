# Troubleshooting

As I'm sure Brandon above has just mentioned, we've just recently pushed out a new beta for playtesters! Most of what I've been doing over the past couple weeks has been bug-fixing, mainly - fixing a few things before the new beta release, and then fixing some more stuff past then. We've also just recently just upgraded the engine from Unreal Engine 4.22 to 4.25, which required, you guessed it, more fixing.

So yeah, a pretty dull couple weeks for me, but here's a couple highlights I guess.

**Upgrading from 4.22 to 4.22**

So I like to work on Linux myself. Epic don't provide any downloads for the engine on Linux, so I end up having to compile it myself... which is fine. It takes a long time to do, but otherwise is a pretty easy process so I'm ok with it.

So, I go ahead and merge in the 4.25 changes into my local copy of the engine's source code, trash the old build, and start building anew. My computer starts chugging for a... a while... a few hours in fact, and after all that it's done, I go to launch the editor, aaaand... it says it's launching 4.22 again.

Turns out, the merge failed and I never merged in the 4.25 source code - that was a *fun* few hours wasted.

**The game black-screens when launching standalone, but not when playing in the Unreal Editor**

This was a fun one because of it's timing - it started happening not too long before we were planning to release the new playtester beta. So... turns out that during game loading, the engine was trying to load *something* that depended on loading `BP_PlayerBallPawn`, which in turns depended on loading *something else*, which in turn depended on loading `BP_PlayerBallPawn` again! This... for whatever reason... caused the engine to get stuck in some kind of infinite loop presumably, or at least it was hanging somehow. Anyways, that was a dependency chain that needed to get broken in some way.

Soo yeah, there's a couple stories for ya - nothing too exciting this time round.
