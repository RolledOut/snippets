# A preview of previews

Y'know what the level-select menu has been missing the whole time? Preview images of the levels! You can't just expect me to remember what each level is by its name, right? Problem is...we have a *lot* of them - a few hundred levels. You miiight be able to convince me to go through and take screenshots of all of them by hand, if you were persuasive enough and I was in a good mood, but y'know what's a better idea...? Automating it!

After all, we already had systems in the game to a: flip through all the levels in a course, and b: do a spin-in cutscene showcasing the whole level for a few seconds before you started playing it. We could just reuse that, right?  And thus, the "screenshot" game mode was born - a hidden game mode in addition to the not-so-hidden arcade and practise modes.

It's certainly not the prettiest mode, though not that it really matters. Launching into screenshot mode with a course will lock the camera's field-of-view to 50, start playing the spin-in cutscene, then after a few frames, save a 512x512 screenshot in the level folder named `preview512.png`. The next frame, the next level in the course is loaded, then a few frames later it takes a screenshot of that, etc, etc. That's it! At the end you can even try to play on the last level - I'm just using the same 'ol spin-in and spawning logic since, well, it just worked for taking screenshots - no need to do anything special for a developer mode.

<video controls playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/29/screenshot.mp4" type="video/mp4">
</video>

Now that we've got the screenshots, all that's left to do it, well, load them on the level-select menu. A fairly simple task, made a little bit complicated by the fact that we don't want loading preview images to freeze the game for two or so seconds when flipping between the different worlds in the menu. A little bit of fenangling with loading images on multiple threads later though, aaaand...

![Practice mode level select](https://gitlab.com/RolledOut/snippets/raw/master/blog/29/level_select.png)

...tadaa! Nice.
