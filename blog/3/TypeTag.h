// Do what you want with this, I don't mind
// -CrafedCart

#pragma once

#include "UnrealString.h"

// P stands for Propertized
// ...although it doesn't quite make as much sense to keep with the P prefix now that I'm making this more general
// but whatever

template<typename T>
struct FPMetaType
{
    static const TCHAR* GetName()
    {
        return TEXT("[UnregisteredType]");
    }
};

/**
 * Types declared as propertized can have their name fetched at runtime<br>
 * This is used for more helpful error messages
 */
#define P_DEFINE_META_TYPE(T) \
    template<> \
    struct FPMetaType<T> \
    { \
        static const TCHAR* GetName() \
        { \
            return TEXT(#T); \
        } \
    };

#define P_ENABLE_META_TYPE() \
    public: \
    virtual const FTypeTag* GetTypeTag() const { return FTypeTag::FromInstance(*this); } \
    private:


// Some default primitive/UE4 types
P_DEFINE_META_TYPE(int32);
P_DEFINE_META_TYPE(float);
P_DEFINE_META_TYPE(FString);

// Fwd declaration
class FTypeTag;

// Template voodoo magic
// Something something SFINAE
/**
 * Check whether a type has a const FTypeTag* GetTypeTag() const method
 */
template<typename T>
class FHasGetTypeTagFunc
{
private:
    typedef uint8 YesType[1];
    typedef uint8 NoType[2];

    template<typename U, const FTypeTag* (U::*)() const>
    class Check {};

    template<typename C>
    static YesType& Test(Check<C, &C::GetTypeTag>*);

    template<typename C>
    static NoType& Test(...);

public:
    static const bool Value = sizeof(Test<T>(0)) == sizeof(YesType);
};

class FTypeTag
{
public:
    const TCHAR* Name;
    SIZE_T Size;

private:
    FTypeTag(const TCHAR* InName, SIZE_T InSize) : Name(InName), Size(InSize) {}

public:
    template<typename T>
    static const FTypeTag* From()
    {
        static FTypeTag Tag = FTypeTag(FPMetaType<T>::GetName(), sizeof(T));
        return &Tag;
    }

    template<typename T>
    static typename TEnableIf<FHasGetTypeTagFunc<T>::Value, const FTypeTag*>::Type From(T* Object)
    {
        return Object->GetTypeTag();
    }

    template<typename T>
    static typename TEnableIf<FHasGetTypeTagFunc<T>::Value, const FTypeTag*>::Type From(T& Object)
    {
        return Object.GetTypeTag();
    }

    template<typename T>
    static typename TEnableIf<!FHasGetTypeTagFunc<T>::Value, const FTypeTag*>::Type From(T* Object)
    {
        return FTypeTag::From<typename TRemoveCV<T>::Type>();
    }

    template<typename T>
    static typename TEnableIf<!FHasGetTypeTagFunc<T>::Value, const FTypeTag*>::Type From(T& Object)
    {
        return FTypeTag::From<typename TRemoveCV<T>::Type>();
    }

    template<typename T>
    static const FTypeTag* FromInstance(T& Object)
    {
        return FTypeTag::From<typename TRemoveCV<T>::Type>();
    }
};

// Thank you https://codereview.stackexchange.com/a/70112
// and also http://www.axelmenzel.de/articles/rtti
