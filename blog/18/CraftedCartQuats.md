# The *joy* of quaternions

Quaterni-what-now?

So, there's a few ways you can represent rotations in 3D space - perhaps the easiest one to explain would be Euler angles.  Euler angles are super simple: rotations are represented as a rotation along the X axis, followed by a rotation along the Y axis, followed by a rotation along the Z axis... or maybe a rotations along the Z axis then X then Y... or maybe something else - the order can be different between different bits of software.

Quaternions are another way of representing rotations, and a far less intuitive one at it! I'm not a mathemagician, despite what this pencil I got from a secondary school math teacher may say, so I don't quite fully understand them myself, but from what I've gathered, they're a way of representing rotations with four components X, Y, Z, and W - something along the lines of storing an axis to rotate around, plus an angle, plus some more extra steps.

![I couldn't find that pencil, so here's an artist's rendition](https://gitlab.com/RolledOut/snippets/raw/master/blog/18/pencil.jpg)

Anyways, there's a few other ways of representing rotations, but those two are the most common ones you'll find around when it comes to working with game dev, computer graphics, and more. For Rolled Out! stages, we use quaternions - why quaternions instead of the much simpler Euler angles you may ask? Well I'll let this video explain why.

<video controls autoplay muted loop playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/18/rotations.mp4" type="video/mp4">
</video>

Follow the tip of the arrow - note how when I set it to rotate with quaternions, the arrow moves in a straight path towards its target - with Euler angles, it takes a bit of a curvy detour to get to the target. Trying to animate with Euler angles doesn't always give you what you may expect!

Another issue with Euler angles is something called "gimbal lock".

<video controls autoplay muted loop playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/18/gimbal_lock.mp4" type="video/mp4">
</video>

It's possible to rotate 90 degrees on one axis with Euler angles, and then just lose an axis of rotation! See how after I rotate 90 degrees on the X axis, the Y and Z axes line up and rotating either of them rotates the monkey in the same way! Wuh oh...

([This](https://www.youtube.com/watch?v=zc8b2Jo7mno) is a good video if you want to learn more about gimbal lock)

Soo... that's why we're using quaternions.

Anyways, I've just been tinkering with stage animations as of recent. I've talked about how we use animation curves now to interpolate objects from point A to point B - rotations throw a bit of a spanner into the works. The way I've been setting up the whole animation curves stuff makes it easy to just animate any single numerical property on an object, like an object's X position or Y scale. With quaternion rotations however, it generally doesn't make much sense to individually animate the X, Y, Z, and W components one-by-one. What's typically done instead is a thing called "spherical linear interpolation", or "slerp" for short. You take a start quaternion rotation and an end quaternion, as well as an "alpha" number, telling us how far we want to interpolate from the start rotation to the end rotation. Feed that all in to the slerp function and you get another rotation out.

We're still gonna have curves to let you define how an object should rotate from rotation A to rotation B, it's just gonna look a bit funky though as you'll be animating that "alpha" value instead.

![How quaternion curves work](https://gitlab.com/RolledOut/snippets/raw/master/blog/18/quat_curve.png)

So, unlike position and scale keyframes where the Y axis on the graph represent a position or scale factor, for rotations the Y axis represents an interpolation percentage. Each keyframe has a quaternion associated with it, and the curves between keyframes represents in what way we want to animate from the previous rotation to the next rotation.

Anyways that's what I've been tinkering with as of recent. I may present all this in a coherent (I hope) way, but my thoughts sure as heck have been anything but coherent in the past few days. >.<

*rotations have always haunted me in this game - they've always been a right pain to work with*
