# Fisix

Not a whole lot to report on from me this time - the past couple of weeks I've mostly just been tinkering around with the quaternion stuff I mentioned in the last blog post, as well as doing some cleanup on the physics code to hopefully make it easier to add objects with custom collision responses (like how bumpers should impart a greater velocity onto the ball compared to just smacking into a wall). That's all been... well, dull at best, irritating at worst, but hey some things you've just gotta do.

Previously, while we had used our own custom physics for testing for ball collisions against stage objects, we had relied on Unreal Engine's built in physics code (which in turn uses Nvidia PhysX) for testing bumper/goal/other "special object" collisions, just because it was *easy*, with the plan of replacing it later.

The main real issues with relying on Unreal's physics code is that it's non-deterministic, and framerate-dependent. Now, this is fine for many games - determinism isn't all too important if you don't care about being able to replay inputs and get frame-perfect results, and having framerate-dependent physics means that weaker computers can compute physics less frequently, whereas beefier machines can have more accurate physics by computing it more often.

Anyways, our physics code had been contained almost entirely in one place for a while: inside the player ball. This has been, well, just fine for quite a while now - though now that we're looking to add support for "special objects" into our own physics code... well perhaps it's time for a little bit of cleanup. It'd sure be ideal if we didn't have to shoehorn in code for seesaws, bumpers, goals, etc. all into the player ball, but rather into the seesaw/bumper/etc. objects themselves.

So, the new plan is *every* object will be able to have custom collision/physics code attached to it, as well as a "collision priority" value. Each tick of the game, the player ball will go through every object in the stage and fetch its collision priority, before sorting the objects based on that. Why have collision priorities? Well objects that are gravity surfaces should be tested for collision before other objects, to make sure the gravity direction gets adjusted before other objects have a chance to potentially move/depenetrate the ball away from said surfaces.

After sorting, the ball will then ask each object to attempt ball collision. Many objects will simply do nothing in this phase, either because the ball isn't colliding with the objects, or because the object has no collision behaviour - some objects however will want to do something to the ball, such as moving it to a new position (useful for depentrating the ball out of objects, or for teleporters), setting its velocity (used for when you bonk off of a surface, or for friction calculations), setting its gravity direction, etc. These objects create various "collision response" messages and sends them off to the ball, which then goes ahead and handles the messages.

```rust
// On a scene node
fn TryBallCollision(...) { ... }

enum RoCollisionResponse {
    /// Teleport the ball to a new position (and do not test for collision on the path between the current and new ball
    /// positions)
    ///
    /// This would typically be used for depenetration out of objects, but can also be used for wormholes.
    MoveBallAbsolute {
        NewPos: FVector,
    },

    /// Pretty self-explanatory - can be used for bumpers, goal tape, mines
    SetBallVelocity {
        NewVelocity: FVector,
    },

    /// Changes the target gravity direction of the player ball
    SetBallGravityTarget {
        NewGravityDown: FVector,
    },

    /// This one is used to help figure out how much the ball should be rotating visually - it has no effect on gameplay
    /// (apart from looking nice)
    HitGround {
        GroundSpeed: FVector,
        GroundNormal: FVector,
    },
}
```

That's pretty much what a collision response message looks like... or, what it would have looked like if we wrote the game in Rust. The actual C++ code for this is a fair bit more convoluted since C++ doesn't have support for algebraic types.

Anyways, that's pretty much the summary of the changes I'm making the physics. Hopefully after this we should really be able to get core features nailed down again given this is the real last major *blocker* for now.
