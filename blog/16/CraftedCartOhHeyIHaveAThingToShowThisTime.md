# Getting somewhere

Ok let's get thing started something visual this time for all you non-code-heads.

<video controls autoplay muted loop playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/16/getting_somewhere.mp4" type="video/mp4">
</video>

This is what we've got after a very, *very* long time of code cleanup. While a lot of my previous blog writeups have just been talking about various concepts in code without actually having tangible content to show, the video here this time can be thought of as a summary of what I've been talking about for the past months, kind-of.

And now that I've shown you a worse version of the game, it's time to explain why this is a better version of the game... theoretically... ignoring the broken camera.

1. Massive decoupling of game elements

Rolled Out, as it is in the current beta release, is a very fragile game. Everything depends on everything else to function, and we've just been relying on Unreal Engine to try and spawn and start running code from objects in the world in the right order. Pretty much everything, as soon as it spawned in, tried to grab references to other objects in the world...
- The stage tries to get the ball to figure out how it should tilt itself visually
- The stage tries to get a map mesh manager to figure out what world-specific variants of goals and collectables to use
- The ball tries to get the stage to figure out what it's colliding against
- The goals try to get the ball to figure out when to trigger a level victory
- The minimap tries to get the ball to figure out where to position its camera
- A lot more

![EVERYTHING DEPENDS ON EVERYTHING](https://gitlab.com/RolledOut/snippets/raw/master/blog/16/spaghetti.png)

Now importantly, the order that stuff is spawned in is not guaranteed to be the same every time. As you can imagine, thiiiis broke some stuff. Our *"solution"*, just get objects to wait 200 milliseconds or something like that before trying to grab references to everything else.

...which meant if your computer took too long to load in the gameplay world, the game would break...

It also broke down when it came to multiplayer, where now you have to deal with multiple balls and the code never grabbing the right ball.

So now, nothing will be trying to grab references when they're spawned in, and we won't just be spawning everything at once and hoping everything just figures-it-out. Code responsible for spawning in objects will also be responsible for making sure said spawned objects have valid references to whatever it needs to function. We're also un-spaghettifying the web of dependencies (wh..why does the spin-in camera spawn the minimap?) - the ideal being only having one-way dependencies (which can be accomplished with simple changes such as having the ball control the stage tilt instead of the stage tilting to whatever inputs it finds on the ball).

2. C++-ifying code

I'm sure I've mentioned this a few times before, but there's been a lot of moving code over from Unreal's visual scripting "blueprints" to C++. Stuff becomes *a lot* more maintainable when you're just working with text - plus there's a performance boost too.

Just recently the last of the physics code got moved over to C++-landia. While most of the heavy lifting (ball depenetration and collision response) was already done there, certain parts like actually telling the ball to move to your input and some handling of gravity surfaces still resided in blueprints. Well no more! Up next on the list is the camera code (which, as you can see from the video... not quite there yet).

3. More modularization

Rolled Out! has now been split into three different modules, `PolarCore` (which contains generic reusable game code), `RoCore`, and our our main game module. In [dev update 12](https://blog.rolledoutgame.com/2020/01/15/dev-update-12/) I mentioned wanting to be able to simulate the game without having to launch the entirety of Unreal Engine. All of our game simulation logic has been moved out to `RoCore` now, which with one or two minor exceptions can function on its own without needing a full Unreal world and environment and all. The stuff that used to be in our main game module is now tasked with setting up a world to run the game in and providing visual representations of the ball and stage and what now being simulated with `RoCore` - some "Unreal Engine glue" as I like to call it.

Ahhh it's so nice to see this whole thing starting to come together. That's about all I've got to say for this blog post - it probably means a lot more to me than it does to you, but hey I'm sure some programmers out there will get my excitement for this.
