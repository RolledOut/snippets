# Some general tidbits
Not a whole lot of interest has been going on with regards to the code side of things (besides getting rather frustrated with initialization and templates and function pointers and aargh in C++), so this fortnight I'm just going to be going over some miscellaneous smaller bits and pieces that help make the code base nicer to work with and make me feel less like I want to rip my hair out.

## Iron Oxide
Rust! For those who haven't heard of it, [Rust](https://www.rust-lang.org/) (not to be confused with [Rust](https://rust.facepunch.com/), or [Rust](https://en.wikipedia.org/wiki/Rust)) is a programming language unlike most others. Its main premise is a strong focus on safety with minimal overhead, and it achieves through analyzing code during compilation, and rejecting if it the compiler deems it as potentially unsafe.

I've been tinkering around with Rust as of recent, making a StepMania font texture generator for [a theme I've been making](https://gitlab.com/CraftedCart/permafrost_sm5_theme/), because [StepMania's own generator doesn't handle some foreign characters at all](https://i.imgur.com/GT1dups.png).

![StepMania Font Generator](https://gitlab.com/RolledOut/snippets/raw/master/blog/9/sm_font_gen.png)

Anyways, what does it mean for code to be "safe" or "unsafe"? That's a broad term, but a few examples of dangerous procedures include...
- Bad memory access/memory leaks - Languages like C and C++ make the programmer have to think about allocating and deallocating chunks of memory. It is really, *really* easy to mess this up, such as trying to access access a bit of memory has been deallocated, crashing the program. Forgetting to deallocate memory on the other hand leaks memory.
- Race conditions - In multithreaded code (that is when multiple pieces of code run simultaneously across your CPU cores), it is possible for multiple threads to try and access the same bit of memory simultaneously. Well what happens if one thread is midway through writing to a bit of memory while another thread is midway through reading it?
- Lack of error handling - It is far too easy to miss error handling sometimes, like forgetting to check an error code. Usually this results in bad memory access later on down the line, as a function returns invalid data or sets some invalid state, and then you try to use such data later on.

So, what's this got to do with Rust and Rolled Out? Well I find that Rust handles these issues rather well... so well that I've taken some ideas from it and implemented it in our C++ code base for Rolled Out!

## `TResult`
Rust almost always forces you to handle errors. Functions that can error in rust will usually return a `Result` object, which can contain a successful result from computation, or an error. If you want to make use of the successful result, you're forced to "unwrap" it from the result object, handling errors in the process.

```rust
// Try to open "hello.txt" - this returns a result
let result = File::open("hello.txt");

// Set the variable "f" to the file handle if opening succeeded, otherwise crash with a message if we failed
let f = match result {
    Ok(file) => file,
    Err(error) => {
        panic!("Problem opening the file: {:?}", error)
    },
};
```

Even if there is no way to handle an error well without terminating the application, terminating as soon as we know something went wrong, with a message too, is easier to troubleshoot for both us as developers, and end users. It's easier for developers as it's far easier to see what went wrong if we crash at the source of the problem, rather than later on in the code execution, and it can be easier for end users as logging an error message can help them determine what the issue is if there isn't a problem with the code.

On the C++ side of things, I found [this](https://github.com/oktal/result) excellent implementation of a Rust-style Result type for C++, which I've taken in to our Rolled Out! code base and modified a bit to work nicer with UE4 and our own error types. Kudos to [oktal](https://github.com/oktal) and [Nashenas88](https://github.com/Nashenas88) for their work on that. Now we can handle errors as such.

```cpp
TResult<TSharedRef<FStageConfig>, FGameErrorRef> Res = FStageConfigParser::ParseStageConfigFromFile(ConfigPath);

if (Res.IsOk())
{
    TSharedRef<FStageConfig> Config = Res.Unwrap();
    // Do stuff with the config
}
else
{
    FGameErrorRef Err = Res.UnwrapErr();
    // Handle the error somehow (perhaps tell the player that this config is invalid)
}
```

In the event that the game cannot continue due to an error (such as not having permissions to read the local game database file), `Res.Expect(ErrorMessage)` works to unwrap the success object or crash the game if there was an error. We'll even show you what went wrong before crashing.

![An error dialog](https://gitlab.com/RolledOut/snippets/raw/master/blog/9/expect_failed.png)

Hopefully you'll never have to see anything like that though.

## `TThreadChannel`
When it comes to multithreading, there are a couple ways to communicate between different threads. Approach number one is with shared memory: one thread can modify a bit of memory, and another thread picks up on the change and acts accordingly. This works well but only if you can guarantee that multiple threads aren't modifying and reading the same chunk of memory simultaneously ([Mutexes](https://stackoverflow.com/questions/34524/what-is-a-mutex) help with that).

Another approach is to have threads communicate with each other through channels. Imagine an office where you've got a stack of papers. Various threads may add more paper to the top of the stack, and you take jobs from the bottom of the paper stack, and when you're done with your stack of paper you wait patiently for threads to add more work to your pile, because you are a very boring employee.

Ok I'm not quite sure that that analogy was, but that's the concept of an MPSC (Multiple Producer, Single Consumer) channel. Multiple threads send data into a receiving thread's job queue, when then processes the results. In Rust, there are separate sender and receiver objects, with one thread having control of the receiver and multiple threads each having their own sender. For Rolled Out, I've just made a single "ThreadChannel" object: there wasn't really a need to split it up into a sender and receiver given I don't have a compiler to bargain with (The Rust compiler can be a very strict teacher).

I've just recently been overhauling stage indexing. Previously, Rolled Out! only ever spun up one thread to index all stages, which just interacted with the game database directly (with shared memory). Now, Rolled Out! will index stages across many different threads - in my case, across 12 threads given I have a 12 thread CPU. When each thread is done with making sense of stage metadata, they will send their results down a thread channel to a 13<sup>th</sup> thread, whose sole responsibility is to take the results from all 12 threads and shove them one-by-one into the game database.

![Channels](https://gitlab.com/RolledOut/snippets/raw/master/blog/9/channels.png)

This, combined with various other factors (JSON stage configs, competence with using a database...) speeds up stage indexing massively. I'm kinda reluctant to give numbers at the moment given it's not finished (Currently this only adds stages to the index, it doesn't check for deleted or modified stages) but we're sitting around 1.5 to 2 seconds of indexing, even on a hard drive, down from about 30s to a minute with the previous method. Again, take these numbers with a grain of salt, though I wouldn't expect them to rise too much.

Anyways, that's about all I have for this post.
