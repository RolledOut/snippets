# I don't know what to title this section ¯\\\_(ツ)\_/¯

Yet again gonna start off with a video for y'all.

<video controls autoplay muted loop playsinline>
  <source src="https://gitlab.com/RolledOut/snippets/raw/master/blog/17/refactor_playground.mp4" type="video/mp4">
</video>

*The camera is almost there this time... just... turning the wrong way round sometimes.*

Anyways, this is gonna be another miscellaneous tidbit from me this time. It's hard to think of interesting or big-picture stuff to write about when most of what's been going on is tinkering with nuances and implementing stuff you've already seen but in a less fragile way.

## Lifetimes

Consider the scene where you're making a custom level and want to add some scripted functionality with Lua, and for *some* reason you end up typing this.

```lua
local start_platform = stage:get_object("mesh_start_platform")
local position = start_platform.transform.position

start_platform:remove()

position.x = 42.8
```

> We haven't drafted out a Lua API yet, don't expect the functions and stuff here to be named/used the same way here as in the final release

If you aren't familiar with Lua, that code sample above gets a reference to an object in the level, gets a reference to that object's position, *removes* the object from the world, then tries to set the X location of the now non-existent object.

Now there's an obvious solution this problem: the script should raise an error when you try and set the X position on an non-existent object. The trickier question is how do we know whether an object exists or not? You see, with C++, there is *no* way to reliably tell, given a chunk of memory, whether that memory contains a valid object or not: once an object is deallocated, it becomes uninitialized memory ready for something else to use. No references to said object get updated *by default*...

...by default... To help with memory management, some people (including us) use "smart pointers" - smart pointers own a chunk of memory, and only deallocate the memory under certain conditions (For example a "shared pointer" only deallocates memory when there are zero shared pointers to a single chunk of memory left). More notable for our purposes, there are "weak pointers". Weak pointers point to a chunk of memory owned by a shared pointer, but do not take any ownership responsibility for the memory. When a shared pointer realizes it's time to deallocate the chunk of memory, it *also* goes ahead and nullifies weak pointers - so if you have a weak pointer to an object, you can tell if it points to valid memory or not, since it will either contain a memory address with valid memory, *or* null.

So, how does this help us with Lua? Well, I'm making a rule here that anything exposed to Lua from our C++ codebase should have a lifetime attached to it. In the above example, we manage the memory of stage objects with shared pointers - that there defines a lifetime boundary. Lua attempts to get the transform of the `start_platform` object. Since `transform` is not managed by a shared pointer, it inherits the lifetime of `start_platform` (by storing a weak pointer to `start_platform`). The same goes for `position` - it inherits the lifetime of `transform`, which inherits the lifetime of `start_platform`.

Removing the start platform object causes its lifetime to come to an end. Since `transform` and `position` use the same lifetime as start platform, those references become invalidated in addition to the `start_platform`. Trying to access an object whose lifetime has come to an end can be detected from C++-land, and we can put a stop to that rather than potentially crashing the game, or worse, exposing a security vulnerability (as Lua should not be able to read/manipulate the contents of memory that it should no longer have access to).
