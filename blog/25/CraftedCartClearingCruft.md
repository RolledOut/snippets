# Clearing out cruft

Not a whole lot to say again from me this time round - most of what I've been doing has just been a whole lot of plumbing really. Setting up logic for practise mode and arcade mode to say what should happen on-goal, on-fallout, how to load a level when gameplay begins, etc, so we can have some normal gameplay instead of just doing everything in debug menus.

So.. instead of talking about all that boring stuff, how about we take about some different boring stuff instead! ...just for a bit.

There has been... a lot of cruft that's built up over the game's development. I pretty much started ignoring all the warnings that compiling gave me a long while ago since I couldn't be bothered to sift through all the deprecation warnings about old code using other old now-replaced code. Only now have I actually started bothering to get rid of the old cruft now that... some other devs on the team got a tad confused and tried using the old stuff that doesn't work any more.

How does this stuff even accumulate, you ask? Well, at various points during development, it's just been easier to clean up and re-implement stuff from scratch instead of refitting what we already had to suit needs - a lot of just removal of technical debt really. During this time we still keep the old stuff around - it still needs to be there in order for the game to compile - we just...never removed the old stuff when we were done. ;P

...yeeeah that's not really a lot huh. Thinking of interesting stuff to write about for dev blog posts is haard when boring 'ol pluming is what I've been mostly doing as of recent. Welp, that's all from me for now.
