# Morris joins the battle!

...but how?

After much reworking of how stages function, I am back to wrapping my head around networking, and all the pains that come along with it. Specifically, I've been tackling what happens when a player joins a multiplayer name, as well as what happens when switching map (which are essentially the same thing for reasons that I will explain in a bit).

So first of all, let's give a brief overview of how Unreal Engine handles this by default. A game consists of a game mode and a game state, and players consist of a player controller, a player state, and a pawn (their physical presence in the world - in this case, a ball with an animal inside). When a player connects to a server, their client will be given their player controller, player states of all players, the game state, etc. What's important to note is that these objects aren't given all in one go. A player will usually receive their player controller first before anything else, and by default gameplay starts as-soon-as-possible.

Yes, this means gameplay starts *before* the client would have received the player states and the game state and more - some critical objects that would need to be referenced throughout the game - usually resulting in crashes as I write code assuming these objects exist when they don't yet.

![The player connection process when stuff goes badly](https://gitlab.com/RolledOut/snippets/raw/master/blog/7/kaboom.png)

Now why do I say this is essentially the same as when switching map? Well in Unreal, when travelling to a new map (such as from the multiplayer lobby map to the gameplay map), the server will disconnect all clients, the server and clients will all travel to the new map, and all clients will try to reconnect. There is a way to switch map without disconnecting/reconnecting clients (called seamless travel), but I want to say we can't use that because clients may be loading the gameplay map for the first time (The Unreal documentation says seamless travel cannot be used in this case).

So, what's my solution to this issue? Well the short answer is, like many games, there's now a waiting-for-players period.

![A waiting for players screen](https://gitlab.com/RolledOut/snippets/raw/master/blog/7/waiting_for_players.jpg)

As for how the waiting-for-players screen works, keep reading.

First there's the issue that when switching map, players disconnect and reconnect. We need to keep track of how many players there were before everyone disconnected, so we know when everyone has reconnected before starting the game.

In addition, each player now has a ready flag: `bPlayerIsNetworkReady`. After connecting to a server, a client will not be network ready and continuously check whether they have received all the data they need to start from the server. When the client has all the data it needs, it will tell the server that they are network ready, and the server will set the `bPlayerIsNetworkReady` flag accordingly. If all players have reconnected to the server and all players are marked as network ready, the server will start the game.

Finally, there is the issue of what happens if a player never reconnects to the server after switching map or if a player never becomes network ready (which could be caused due to a bad internet connection, an untrustworthy player, or a good 'ol bug)? Well after 60 seconds of waiting for players, the server will forcibly start the game, kicking any connected but non-network-ready players in the process.
