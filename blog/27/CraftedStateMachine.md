# On state machines

State: A necessary part of programming, and while it would be nice to not have deal with managing too much state, it is *very* prevalent within game development.

So: for starters, let's give some examples of what we might want to keep track of for each player in Rolled Out!

- Whether we're in the first intro cutscene (where the camera spins around the level slowly)
- Whether we're in a subsequent intro cutscene (where you're retrying a stage and the camera spins around much quicker)
- Whether we're in-gameplay
- Whether we've gone out-of-bounds
- Whether we've goaled
- Whether we've run out of time
- How long we've been playing the intro/goal/out-of-bounds/etc. cutscene (so we know when to stop the cutscene)
- How many collectables you've picked up
- How long is left on the timer
- Etc...

Now, I'm just gonna focus on the first six here (the ones beginning with "Whether"). One thing to note about these six is that one one of those things can be active at any given point in time (so, for example, you could have goaled, **or** you could be in the first intro cutscene, but you can't be both at once) - we can create a state machine out of this!

![The player state machine](https://gitlab.com/RolledOut/snippets/raw/master/blog/27/fsm.png)

Simply put, a state machine is just a box that has a certain state (eg: PlayerGameplay), and can be transitioned to different states. For a basic state machine, that's it! If you wanted to, you can do fancier things like doing stuff when transitioning between states, having states store some data for themselves - those might be things we would want to look in to in the future if stuff starts getting out of hand, but for now we just have a super-barebones version where you can just switch state and that's that.

Perhaps unsurprisingly, a barebones state machine has quite a simple implementation. For starters, we need some place to define all of our states...

```cpp
namespace EPlayerMode
{
    enum Type
    {
        WaitingToStart, //< Waiting for BeginPlay
        PlayerGameplay,
        PostGoalCrossed,
        PostFallout,
        PostTimeOver,
        SpinInFirst,
        SpinInRetry,
    };
}
```

...yup, that'll do. Then, we just need a variable to store the state, and some way to change the state stored.

```cpp
class ROCORE_API FRollingCtrl : public FBaseCtrl
{
protected:
    EPlayerMode::Type Mode = EPlayerMode::WaitingToStart;

    // -- snip --

protected:
    void SwitchMode(EPlayerMode::Type InMode)
    {
        Mode = InMode;

        // Enable/disable input based on whether we're in PlayerGameplay mode
        GetLocalPlayerEventBus().SendImmediate(FLocalPlayerSetInputEnabledEvent(Mode == EPlayerMode::PlayerGameplay));
    }
};
```

That'll do it!

Now... why have a state machine in the first place? For... a long while in the earlier days of the game, managing state was.. a bit of a mess - mostly involving various boolean variables, or sometimes not bothering at all. For example, the way fallouts and goaling used to work is when you goaled, some code would run, and after a fixed delay some more code would run to restart the stage, or whisk you off to the next level. It was entirely possible for you to goal, and then fallout since... it never occurred to us to check that we had goaled before falling out.

Anyways, my point is having all these mutually exclusive states just in a single variable is way easier to manage over just tacking more and more stuff on over time. When we pass through the goal, we check that the state machine is in the `PlayerGameplay` state before doing anything. That's a lot easier to manage over checking stuff like `not GoalCrossed and not FallOut and not TimeOver` - and if for some reason in the future we want to add another failure state, we don't need to bother updating the goal checking code to say `... and not YouPickedUpSoManyCoinsThatYouGotTooHeavyAndPunchedAHoleInTheStage`.
