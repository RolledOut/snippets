# Do you have your bus pass?

A *lot* of our efforts over the past few months has been about getting the ball and stage working and interacting with each other in a sane way, arguably a big part of the game, but definitely not enough to make up a whole gameplay experience. As of recent, I've been tinkering with trying to make player controllers, figuring how different parts of the player controller will interact with each other, and trying to prepare the game for networking.

...but first, I should probably clarify what I mean by a "player controller". While ball and stage interaction is great, what should happen when you, say, go out of bounds, or cross the goal? In practise mode, the answer would be to respawn the ball in both cases; for arcade mode you'd subtract a life and respawn for the former, or move on to the next stage for the latter; in the main game there are no lives but you should respawn, etc. Different modes would have different player controllers here, containing logic to dictate what a player has (Do we have lives? Do we need to keep track of progression through arcade mode? etc.) and what should happen in response to various events (like the aforementioned falling out of bounds, or goaling).

> Side note, I know Unreal Engine gives you "player state" and "game mode" objects which sound like they'd be better suited for this... to which I say, yeah, they might be! When you goal, the server should be able to detect that and update some state/notify the player of what should happen next.
>
> In the end, I've kinda just decided to ignore a large part of Unreal's game framework. Given the nature of custom physics handling, combined with the fact that in some multiplayer games, players will each be on all various different levels, asking the server to oversee all that would be really infeasible resource-wise.

A little while ago, I sketched out this diagram as a rough idea of how to take care of organizing player controller stuff.

![The sketch](https://gitlab.com/RolledOut/snippets/raw/master/blog/23/RoNet.png)

The real heart of it is in the middle, with "Ctrl" objects (naming stuff is hard, ok!) - practise-mode, challenge-mode (arcade-mode), and golf-minigame-mode are just the three here I picked as examples. A "Ctrl" object here would be made up of various "Part" objects: so for example the `PractiseCtrl` object may have a `PhysBallPart`, `StagePart`, and a `ScorePart`, but it will not have a `LifePart`, `GolfScorePart`, or `ChallengeProgressionPart`. These parts will predominantly communicate over the "local player event bus" - for example, when the ball recognises that it has gone out of bounds, it will send a message "Hey I'm out of bounds!". At that point, the `LifePart` may pick up on that and subtract a life (if this was arcade-mode), and the "Ctrl" object might decide to respawn the ball if it wants.

If you've ever done game dev before, you may have come across the idea of an "Entity-Component System" (or ECS for short), where "entities" are created by composing various "components" together. In this case, "Ctrl" objects are the entities, and "Part" objects are the components.

Here, there's also the "remote player event bus". The idea is that when we get round to implementing multiplayer, on every client, every player will have their own "Ctrl" object spawned for them. Local players can send messages like "I've moved to this new location" or "I'm on stage qwertyuiop now!" over the remote player event bus, which would then be sent over the network to inform other clients that your player ball should move or whatever. That's connected to the Unreal Engine PlayerController here since networking in Unreal can only be done in a few places: the player controller being one of them.

So... that's what I've been working on over the past while - more specifically the practise mode "Ctrl" object, "Parts" required for it, getting them communicating over the local player event bus, and getting all this showing up and working in-engine as opposed to just something that exists in code-land that isn't visible in-game.
